package com.ahujaBros.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.src.ahujaBros.R;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by insonix on 29/5/17.
 */

public class ItemDialogDetailAdapter extends PagerAdapter {
    ProductsItem mPaths;
    Context mCtx;
    LayoutInflater inflater;
    String[] paths;

    //TO be used in dialog class
    public ItemDialogDetailAdapter(Context context, String[] path) {
        mCtx = context;
        paths = path;
    }

    @Override
    public int getCount() {
        return paths.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        inflater = (LayoutInflater) mCtx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_dialog_item, container,
                false);
        ImageView imgflag = (ImageView) itemView.findViewById(R.id.imgView);
        imgflag.setAdjustViewBounds(true);
//        imgflag.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(mCtx).load(paths[position]).resize(550,600).into(imgflag);
        imgflag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                initDialog(container.getContext());
            }
        });
        container.addView(itemView);
        return itemView;
    }

    private void initDialog(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_full_img);
        ViewPager fullImg = (ViewPager) dialog.findViewById(R.id.viewPager);
        CircleIndicator indicator = (CircleIndicator) dialog.findViewById(R.id.indicator);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        fullImg.setAdapter(new ItemDialogDetailAdapter(context, paths));
        indicator.setViewPager(fullImg);
        dialog.show();
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }

}
