package com.ahujaBros.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.ahujaBros.model.home.GroupItem;
import com.ahujaBros.utils.HomeItemClickListener;
import com.ahujaBros.utils.SortItemClickListener;
import com.github.demono.adapter.InfinitePagerAdapter;
import com.jakewharton.salvage.RecyclingPagerAdapter;
import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 24/5/17.
 */

public class TestAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<String> imageIdList;
    ArrayList<GroupItem> mCatlist;
    private int size;
    private boolean isInfiniteLoop;
    private HomeItemClickListener listener;
    int parentListPosition;
    public static Dialog catDial;

    public TestAdapter(Context context, ArrayList<GroupItem> catlist, List<String> imageIdList, HomeItemClickListener listener, int position) {
        this.context = context;
        this.imageIdList = imageIdList;
        this.size = imageIdList.size();
        mCatlist = catlist;
        this.listener = listener;
        isInfiniteLoop = false;
        parentListPosition = position;
    }

    @Override
    public int getCount() {
        // Infinite loop
        return isInfiniteLoop ? 3 : imageIdList.size();
    }

    /**
     * get really position
     *
     * @param position
     * @return
     */
    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup container) {
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            holder.imageView = new ImageView(context);
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            ViewGroup.LayoutParams lp = new ViewPager.LayoutParams();
            lp.width = ViewPager.LayoutParams.MATCH_PARENT;
            lp.height = ViewPager.LayoutParams.WRAP_CONTENT;
            holder.imageView.setLayoutParams(lp);
            view = holder.imageView;
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Picasso.with(context).load(imageIdList.get(position)).error(R.drawable.not_avlbl).fit().into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catDial = new Dialog(context);
                catDial.setTitle("Select Category");
                catDial.setContentView(R.layout.list_dial);
                ListView list = (ListView) catDial.findViewById(R.id.itemsList);
                Log.i(TAG, "onClick: TestAdapter" + parentListPosition);
                list.setAdapter(new CustomListAdapter(mCatlist, listener, parentListPosition));
                if (mCatlist.size() > 0) {
                    catDial.show();
                }
                Log.i(TAG, "onClick: ns");

            }
        });
        return view;
    }

    private static class ViewHolder {

        ImageView imageView;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public TestAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
}
