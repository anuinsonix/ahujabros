package com.ahujaBros.adapter;

/**
 * Created by insonix on 23/5/17.
 */

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ahujaBros.model.home.GroupItem;
import com.ahujaBros.model.home.ResponseItem;
import com.ahujaBros.ui.fragment.HomeFragment;
import com.ahujaBros.utils.HomeItemClickListener;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.ahujaBros.utils.SortItemClickListener;
import com.github.demono.AutoScrollViewPager;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFoldAdapter extends RecyclerView.Adapter<HomeFoldAdapter.PhotoViewHolder> {

    private static final String TAG = "homeAdapter";
    private List<ResponseItem> mDataSet;
    private Context mContext;
    RecyclerItemGenricClickListener mListener;
    HomeItemClickListener listener;

    public HomeFoldAdapter(List<ResponseItem> dataSet, Context context, HomeFragment homeFragment) {
        mDataSet = dataSet;
        mContext = context;
        mListener = homeFragment;
        listener=homeFragment;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_list_cover, parent, false);
        return new PhotoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PhotoViewHolder holder, final int position) {
        final ArrayList<GroupItem> Catlist=new ArrayList<>();
        final List<String> path = mDataSet.get(position).getCategoryImage();
        Log.i(TAG, "onBindViewHolder: " + path.size());
        for (int i = 0; i < mDataSet.get(position).getGroup().size(); i++) {
            Catlist.add(mDataSet.get(position).getGroup().get(i));
        }
        holder.textviewCover.setText(mDataSet.get(position).getCategoryName());
        final TestAdapter mAdapter = new TestAdapter(mContext,Catlist,path,listener,position);
        holder.imageviewCover.setAdapter(mAdapter);
        holder.imageviewCover.startAutoScroll(3000);
        holder.imageviewCover.setStopWhenTouch(true);
        holder.imageviewCover.setCycle(true);
        holder.imageviewCover.setSlideDuration(4000);
        holder.imageviewCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void refreshData(List<ResponseItem> dataSet) {
        mDataSet.clear();
        mDataSet.addAll(dataSet);
        notifyDataSetChanged();
    }

    protected static class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_cover)
        AutoScrollViewPager imageviewCover;
        @BindView(R.id.imageview_cover_copy)
        ImageView imageviewCoverCopy;
        @BindView(R.id.textview_cover)
        TextView textviewCover;
        @BindView(R.id.share_button)
        Button shareButton;
        @BindView(R.id.parent_frame)
        FrameLayout parentFrame;
        @BindView(R.id.cardLayout)
        CardView cardLayout;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

