package com.ahujaBros.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahujaBros.model.order.OrderHistoryHeader;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 8/6/17.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.OrderViewHolder> {

    ArrayList<OrderHistoryHeader> mItem;
    RecyclerItemGenricClickListener mListener;

    public OrderHistoryAdapter(ArrayList<OrderHistoryHeader> responseItems, RecyclerItemGenricClickListener listener) {
        mItem = responseItems;
        mListener = listener;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_list, parent, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final OrderViewHolder holder, final int position) {
        holder.msg.setText("Order Id: "+mItem.get(position).getOrderId());
        holder.itemName.setText("Total: "+mItem.get(position).getTotalAmount());
        holder.timestamp.setText(mItem.get(position).getDate());
        holder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public void refresh(List<OrderHistoryHeader> data) {
        mItem.clear();
        mItem.addAll(data);
        notifyDataSetChanged();
    }


    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.msg)
        TextView msg;
        @BindView(R.id.item_name)
        TextView itemName;
        @BindView(R.id.content_layout)
        LinearLayout contentLayout;
        @BindView(R.id.timestamp)
        TextView timestamp;
        @BindView(R.id.parent_layout)
        LinearLayout parentLayout;
        @BindView(R.id.main_card)
        CardView mainCard;

        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

