package com.ahujaBros.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahujaBros.model.order.ProductInfo;
import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 27/4/17.
 */

public class OrderDetailAdapter extends BaseAdapter {

    Context mCtx;
    List<ProductInfo> productList;

    public OrderDetailAdapter(Context ctx, List<ProductInfo> productList) {
        this.productList = productList;
        mCtx=ctx;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        LayoutInflater infalInflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.order_detail_row, null);
        TextView product_name = (TextView) convertView.findViewById(R.id.product_name);
        TextView text_mrp = (TextView) convertView.findViewById(R.id.text_mrp);
        TextView supplier_name = (TextView) convertView.findViewById(R.id.supplier_name);
        TextView size = (TextView) convertView.findViewById(R.id.size);
        TextView quantity = (TextView) convertView.findViewById(R.id.quantity);
        TextView text_color = (TextView) convertView.findViewById(R.id.text_color);
        ImageView productImage = (ImageView) convertView.findViewById(R.id.product_image);
        product_name.setTypeface(null, Typeface.BOLD);
        product_name.setText(productList.get(i).getBrandName());
        text_mrp.setText(mCtx.getResources().getString(R.string.Rs)+" "+productList.get(i).getMRP());
        supplier_name.setText("Supplier: "+productList.get(i).getSupplierName());
        size.setText("Size: "+productList.get(i).getSize());
        quantity.setText("Quantity: "+productList.get(i).getQty());
//        text_color.setText(productList.get(i).getColor());
        Picasso.with(mCtx).load(productList.get(i).getProductImage().split(",")[0]).error(R.drawable.not_avlbl).into(productImage);
        return convertView;
    }
}
