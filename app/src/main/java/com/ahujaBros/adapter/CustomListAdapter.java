package com.ahujaBros.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahujaBros.model.home.GroupItem;
import com.ahujaBros.utils.HomeItemClickListener;
import com.src.ahujaBros.R;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 9/6/17.
 */

public class CustomListAdapter extends BaseAdapter {

    ArrayList<GroupItem> list;
    private HomeItemClickListener mListener;
    int parentPos;

    public CustomListAdapter(ArrayList<GroupItem> catlist, HomeItemClickListener mContext, int parentListPosition) {
        list=catlist;
        mListener=mContext;
        this.parentPos=parentListPosition;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position).getGroupName();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.sort_item_list, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.sort_text);
        LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.parent);
        textView.setText(list.get(position).getGroupName());
        rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: position sent"+parentPos);
                mListener.onHomeListItemClick(parentPos,position);
            }
        });
        return rowView;
    }
}
