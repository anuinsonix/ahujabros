package com.ahujaBros.receiver;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.TextView;

import com.ahujaBros.MyApplication;
import com.ahujaBros.fcm.MyFirebaseMessagingService;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.NotificationListener;


public class FcmBroadcastReceiver extends WakefulBroadcastReceiver {


    private static NotificationUpdateListener listener;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent1) {
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName activity = am.getRunningTasks(1).get(0).topActivity;
            Log.e("activity", String.valueOf(activity));

            //TODO update badges as notification reaches
        } catch (Exception e) {
            e.printStackTrace();
        }
        ComponentName comp = new ComponentName(context.getPackageName(),
                MyFirebaseMessagingService.class.getName());
        startWakefulService(context, (intent1.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
        this.context = context;
        if (listener != null) {
            listener.updateUI();
        }
    }

    public interface NotificationUpdateListener {
        void updateUI();
    }

    public void setListener(NotificationUpdateListener listener) {
        FcmBroadcastReceiver.listener = listener;
    }
}
