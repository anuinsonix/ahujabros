package com.ahujaBros.api;


import com.ahujaBros.model.AddToCart.AddToCartResponse;
import com.ahujaBros.model.CategoryDetail.CategoryDetailResponse;
import com.ahujaBros.model.GenericResponse;
import com.ahujaBros.model.Login.LoginResponse;
import com.ahujaBros.model.NotificationResponse.NotificationResponse;
import com.ahujaBros.model.Search.ProductFoundResponse;
import com.ahujaBros.model.Search.SearchKeywordResponse;
import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.cart.OrderResponse;
import com.ahujaBros.model.generic.BadgeResponse;
import com.ahujaBros.model.home.HomeResponse;
import com.ahujaBros.model.menu.MenuResponse;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by insonix on 18/4/17.
 */

public class ApiClient {

    public static final String BASE_URL = "http://ahujabrothers.insonix.com/web_new/";

    public ApiClientInterface getInstance() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(httpClient.build())
                .build();
//
        ApiClientInterface apiService = retrofit.create(ApiClientInterface.class);
        return apiService;
    }


    public interface ApiClientInterface {

        @POST("fetchCategory.php")
        Observable<MenuResponse> fetchCategoriesList();

        @POST("homepage.php")
        Observable<HomeResponse> getHomeData();

        @GET("remove_item_bag.php")
        Observable<MyCart> removeItem(@Query("itemid") String bagId, @Query("uid") String userId);


        @GET("add_to_bag.php")
        Observable<AddToCartResponse> addToCart(@Query("uid") String uId, @Query("pid") String productId, @Query("qty") String qty);

        @GET("getProduct.php")
        Observable<CategoryDetailResponse> getCategoryDetails(@Query("subcatID") String subCatId, @Query("catID") String catId, @Query("departID") String deptId, @Query("sort") String sort);

        @GET("login.php")
        Observable<LoginResponse> Login(@Query("username") String userName, @Query("password") String pwd, @Query("device_id") String deviceId, @Query("device_type") String deviceType, @Query("device_token") String token);

        @GET("my_cart.php")
        Observable<MyCart> fetchCart(@Query("uid") String uId);

        @GET("filters.php")
        Observable<CategoryDetailResponse> filterItems(@Query("group_id") String groupId, @Query("cat_id") String catId, @Query("subGroupId") String subGroupId, @Query("supplier_name") String supplier_name, @Query("min") String min, @Query("max") String max, @Query("sort") String sort);

        @GET("orderDetail.php")
        Observable<OrderResponse> orderCart(@Query("user_id") String uId, @Query("order_detail") String order);

        @GET("getallnotification.php")
        Observable<NotificationResponse> getNotifications(@Query("uid") String uId);

        @GET("badge.php")
        Observable<GenericResponse> readNotification(@Query("id") String notiId);

        @GET("badge_count.php")
        Observable<BadgeResponse> getBadgeCount(@Query("uid") String uId);

        @GET("get_all_keywords.php")
        Observable<SearchKeywordResponse> getKeywords();

        @GET("search.php")
        Observable<ProductFoundResponse> searchKeywords(@Query("search") String search, @Query("sort") String sort);

        @GET("order_history.php")
        Observable<JsonObject> getOrderHistory(@Query("user_id") String uId);

    }
}
