package com.ahujaBros.fcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ahujaBros.receiver.FcmBroadcastReceiver;
import com.ahujaBros.ui.NotificationDetail;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.src.ahujaBros.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "StartingAndroid";
    private NotificationCompat.Builder notificationBuilder;
    private Bitmap img;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getData());
        Log.e("message", "From: " + remoteMessage.getData().get("message"));
        Log.d("tag", remoteMessage.getData().get("tag"));
        String tag = remoteMessage.getData().get("tag");
        String message = remoteMessage.getData().get("message");
        String title = remoteMessage.getData().get("title");
        String icon = remoteMessage.getData().get("icon");
        String notify_id = remoteMessage.getData().get("notification_id");
        img = getBitmapFromURL(icon);
        sendNotification(tag, message, title, icon, img, notify_id);
    }

    private void sendNotification(String tag, String messageBody, String title, String icon, Bitmap img, String notify_id) {
        Random r = new Random();
        int id11 = r.nextInt(9999 - 0 + 1);
        Intent intent = new Intent(this, NotificationDetail.class);
        intent.putExtra("msg", messageBody);
        intent.putExtra("title", title);
        intent.putExtra("id", notify_id);
        intent.putExtra("tag", tag);
        intent.putExtra("icon", icon);
        Log.d("icon", icon);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (tag.equalsIgnoreCase("image")) {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(img))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id11, notificationBuilder.build());
        FcmBroadcastReceiver.completeWakefulIntent(intent);

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


//    //This method is only generating push notification
//    private void sendNotification(String idea_title, String messageBody String sugId) {
////        Intent intent = new Intent(this, MainActivity.class);
////        intent.putExtra("IdeaList", ideasList);
////        intent.putExtra("SugId", sugId);
////        intent.putExtra("PostIdeaChek", "1");
////        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
////                PendingIntent.FLAG_ONE_SHOT);
////
////        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////        notificationBuilder = new NotificationCompat.Builder(this)
////                .setSmallIcon(R.drawable.app_icon)
////                .setContentTitle(idea_title)
////                .setContentText(messageBody)
////                .setAutoCancel(true)
////                .setSound(defaultSoundUri)
////                .setContentIntent(pendingIntent);
////
////        NotificationManager notificationManager =
////                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////        Random random = new Random();
////        int notifyId = random.nextInt(9999 - 1000) + 1000;
////        notificationManager.notify(notifyId, notificationBuilder.build());
//
//    }


}