package com.ahujaBros.utils;

/**
 * Created by root on 24/3/17.
 */

public interface NotificationListener {

    void getBadgeCount(int length);

    void getCartCount(int length);
}
