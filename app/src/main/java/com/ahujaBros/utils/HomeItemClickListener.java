package com.ahujaBros.utils;

/**
 * Created by insonix on 15/6/17.
 */

public interface HomeItemClickListener {

    void onHomeListItemClick(int parentPos, int pos);

}
