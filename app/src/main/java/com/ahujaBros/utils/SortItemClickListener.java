package com.ahujaBros.utils;

/**
 * Created by insonix on 30/5/17.
 */

public interface SortItemClickListener {

    void onListItemClick(int pos);

}
