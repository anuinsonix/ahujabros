package com.ahujaBros.utils;

import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.ahujaBros.model.cart.DataItem;

/**
 * Created by insonix on 30/5/17.
 */

public interface RecyclerItemClickListener {

    void onRemoveItemClick(DataItem pos);

    void onEditIemClick(DataItem pos);

}
