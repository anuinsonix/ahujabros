package com.ahujaBros.BusEvents;

import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.ahujaBros.model.NotificationResponse.DataItem;
import com.ahujaBros.model.home.GroupItem;
import com.ahujaBros.model.home.ResponseItem;
import com.ahujaBros.model.home.SubGroupItem;
import com.ahujaBros.model.order.ProductInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insonix on 26/5/17.
 */

public class Events {


    public static class CategoryToItemDetailEvent {
        public ProductsItem getmItem() {
            return mItem;
        }

        public void setmItem(ProductsItem mItem) {
            this.mItem = mItem;
        }

        ProductsItem mItem;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        String title;

        public CategoryToItemDetailEvent(ProductsItem mItem,String title) {
            this.mItem = mItem;
            this.title=title;
        }
    }

    public static class CatItemEvent {


        public CatItemEvent(ResponseItem mItem, int pos) {
            this.mItem = mItem;
            this.pos = pos;
        }

        public ResponseItem getmItem() {
            return mItem;
        }

        public void setmItem(ResponseItem mItem) {
            this.mItem = mItem;
        }

        ResponseItem mItem;

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        int pos;

    }


    public static class SelectedItemDetailEvent {
        public SelectedItemDetailEvent(ProductsItem mItem, String quantity) {
            this.mItem = mItem;
            this.quantity = quantity;
        }

        public ProductsItem getmItem() {
            return mItem;
        }

        public void setmItem(ProductsItem mItem) {
            this.mItem = mItem;
        }

        ProductsItem mItem;

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        String quantity;

    }

    public static class CartCountEvent{
        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        int count;

        public CartCountEvent(int count) {
            this.count = count;
        }
    }

    public static class CategoriesSearchedEvent{
        public CategoriesSearchedEvent(List<ProductsItem> listFound) {
            this.listFound = listFound;
        }

        public List<ProductsItem> getListFound() {
            return listFound;
        }

        public void setListFound(List<ProductsItem> listFound) {
            this.listFound = listFound;
        }
        List<ProductsItem> listFound;
    }

    public static class CartItemEvent {

        public com.ahujaBros.model.cart.DataItem getItem() {
            return item;
        }

        public void setItem(com.ahujaBros.model.cart.DataItem item) {
            this.item = item;
        }

        com.ahujaBros.model.cart.DataItem item;


        public CartItemEvent(com.ahujaBros.model.cart.DataItem item) {
            this.item = item;
        }
    }

    public static class CategoryEvent {
        List<GroupItem> mGroupItem;
        String mGroupName;
        String mGroupId;
        String mSubGroupId;
        String mCategoryId;

        public ArrayList<SubGroupItem> getTitles() {
            return titles;
        }

        public void setTitles(ArrayList<SubGroupItem> titles) {
            this.titles = titles;
        }

        ArrayList<SubGroupItem> titles;

        public CategoryEvent(List<GroupItem> mGroupItem, String mGroupName, String mGroupId, String mSubGroupId, String mCategoryId, ArrayList<SubGroupItem> titles) {
            this.mGroupItem = mGroupItem;
            this.mGroupName = mGroupName;
            this.mGroupId = mGroupId;
            this.mSubGroupId = mSubGroupId;
            this.mCategoryId = mCategoryId;
            this.titles = titles;
        }

        public List<GroupItem> getmGroupItem() {
            return mGroupItem;
        }

        public void setmGroupItem(List<GroupItem> mGroupItem) {
            this.mGroupItem = mGroupItem;
        }

        public String getmGroupName() {
            return mGroupName;
        }

        public void setmGroupName(String mGroupName) {
            this.mGroupName = mGroupName;
        }

        public String getmGroupId() {
            return mGroupId;
        }

        public void setmGroupId(String mGroupId) {
            this.mGroupId = mGroupId;
        }

        public String getmSubGroupId() {
            return mSubGroupId;
        }

        public void setmSubGroupId(String mSubGroupId) {
            this.mSubGroupId = mSubGroupId;
        }

        public String getmCategoryId() {
            return mCategoryId;
        }

        public void setmCategoryId(String mCategoryId) {
            this.mCategoryId = mCategoryId;
        }
    }

    public static class OrderDetailEvent {
        public List<ProductInfo> getProductList() {
            return productList;
        }

        public void setProductList(ArrayList<ProductInfo> productList) {
            this.productList = productList;
        }

        List<ProductInfo> productList;

        public OrderDetailEvent(List<ProductInfo> productList) {
            this.productList = productList;
        }
    }
}
