package com.ahujaBros.BusEvents;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by insonix on 26/5/17.
 */


public class GlobalBus {
    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}