package com.ahujaBros.presenter;

import com.ahujaBros.model.cart.OrderResponse;

/**
 * Created by insonix on 5/6/17.
 */

public interface BagPresenter{

    void fetchCart();

    void orderCart(String order_detail);

    void removeCartItem(String itemId);

}
