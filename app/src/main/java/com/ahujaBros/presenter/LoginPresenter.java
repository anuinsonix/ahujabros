package com.ahujaBros.presenter;

/**
 * Created by insonix on 31/5/17.
 */

public interface LoginPresenter {

    void loginUser(String userName,String pwd,String id,String type,String token);
}
