package com.ahujaBros.presenter;

import android.util.Log;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.GenericResponse;
import com.ahujaBros.model.NotificationResponse.NotificationResponse;
import com.ahujaBros.ui.NotificationView;
import com.ahujaBros.ui.fragment.NotificationFragment;
import com.ahujaBros.utils.MyPreferences;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 8/6/17.
 */

public class NotificationPresenterImpl implements NotificationPresenter {

    NotificationView mView;
    ApiClient mApiClient;

    public NotificationPresenterImpl(NotificationView view) {
        mView = view;
        mApiClient = new ApiClient();
    }

    @Override
    public void getNotifications() {
        mView.showProgress();
        mApiClient.getInstance().getNotifications(MyPreferences.getInstance().getString(MyPreferences.Key.USERID))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NotificationResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                    }

                    @Override
                    public void onNext(NotificationResponse response) {
                        mView.hideProgress();
                        mView.onLoadingNotifications(response);
                    }
                });
    }

    @Override
    public void readNotification(String notiId) {
        mApiClient.getInstance().readNotification(notiId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        Log.i(TAG, "onNext: badge counter maintained");
                    }
                });
    }
}
