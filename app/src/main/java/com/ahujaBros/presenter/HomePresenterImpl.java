package com.ahujaBros.presenter;

import android.util.Log;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.Search.SearchKeywordResponse;
import com.ahujaBros.model.home.HomeResponse;
import com.ahujaBros.ui.HomeView;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 23/5/17.
 */

public class HomePresenterImpl implements HomePresenter {

    HomeView mView;
    private ApiClient mApiClient;

    public HomePresenterImpl(HomeView view) {
        mView=view;
        mApiClient=new ApiClient();
    }


    @Override
    public void getHomeData() {
        Observable<HomeResponse> categoriesCall = mApiClient.getInstance().getHomeData();
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HomeResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted:  home presenter");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError:  home presenter");
                    }

                    @Override
                    public void onNext(HomeResponse homeResponse) {
                        Log.i(TAG, "onNext:  home presenter");
                        mView.refreshData(homeResponse);
                    }
                });
    }
}
