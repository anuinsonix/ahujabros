package com.ahujaBros.presenter;

/**
 * Created by insonix on 25/5/17.
 */

public interface CategoryDetailPresenter {

    void getCategoryDetails(String subId,String catId,String depttId,String sort);

    void filterItems(String groupId, String catId, String subId, String supplierName, String min, String max, int sort);

    void addToCart(String ProductId, String Qty);

}
