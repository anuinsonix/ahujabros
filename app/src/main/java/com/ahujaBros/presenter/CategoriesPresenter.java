package com.ahujaBros.presenter;

/**
 * Created by insonix on 18/5/17.
 */

public interface CategoriesPresenter {

    void getCategories();

    void getBadgeCount();

    void getSearchKeywords();

    void searchKeyword(String search);

}
