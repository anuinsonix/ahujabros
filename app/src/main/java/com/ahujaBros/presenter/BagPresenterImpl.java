package com.ahujaBros.presenter;

import android.util.Log;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.cart.OrderResponse;
import com.ahujaBros.ui.CartView;
import com.ahujaBros.utils.MyPreferences;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 5/6/17.
 */

public class BagPresenterImpl implements BagPresenter {

    CartView mView;
    ApiClient mApiClient;

    public BagPresenterImpl(CartView mView) {
        this.mView = mView;
        mApiClient = new ApiClient();
    }

    @Override
    public void fetchCart() {
        mView.showProgress();
        Observable<MyCart> myCart = mApiClient.getInstance().fetchCart(MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
        myCart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MyCart>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                        mView.noDataAvailable();
                    }

                    @Override
                    public void onNext(MyCart myCart) {
                        mView.hideProgress();
                        mView.onFetchCart(myCart);
                    }
                });

    }

    @Override
    public void orderCart(String order) {
        mView.showProgress();
        Observable<OrderResponse> myCart = mApiClient.getInstance().orderCart(MyPreferences.getInstance().getString(MyPreferences.Key.USERID),order);
        myCart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OrderResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();

                    }

                    @Override
                    public void onNext(OrderResponse response) {
                        mView.hideProgress();
                        mView.onOrderSuccess(response);
                    }
                });
    }

    @Override
    public void removeCartItem(String itemId) {
        mView.showProgress();
        Observable<MyCart> myCart = mApiClient.getInstance().removeItem(itemId,MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
        myCart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MyCart>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();
                        Log.i(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                        Log.i(TAG, "onError: ");
                        mView.noDataAvailable();
                    }

                    @Override
                    public void onNext(MyCart myCart) {
                        Log.i(TAG, "onNext: "+myCart.getData().size());
                        mView.hideProgress();
                        mView.onSuccessRemove(myCart);
                    }
                });
    }

}
