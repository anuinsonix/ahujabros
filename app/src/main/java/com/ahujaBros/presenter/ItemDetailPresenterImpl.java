package com.ahujaBros.presenter;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.AddToCart.AddToCartResponse;
import com.ahujaBros.ui.ItemDetailView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.fragment.InItemDetailFragment;
import com.ahujaBros.utils.MyPreferences;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by insonix on 31/5/17.
 */

public class ItemDetailPresenterImpl implements ItemDetailPresenter {

    ItemDetailView mView;
    ApiClient mApi;

    public ItemDetailPresenterImpl(ItemDetailView view) {
        mView = view;
        mApi = new ApiClient();
    }

    @Override
    public void addToBag(String ProductId, String Qty) {
        mView.showProgress();
        Observable addTocart = mApi.getInstance().addToCart(MyPreferences.getInstance().getString(MyPreferences.Key.USERID), ProductId, Qty);       //TODO
        addTocart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AddToCartResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                    }

                    @Override
                    public void onNext(AddToCartResponse o) {
                        mView.hideProgress();
                        mView.onAddtoCartSuccess(o.getMessage());
                    }
                });

    }
}
