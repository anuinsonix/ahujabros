package com.ahujaBros.presenter;

/**
 * Created by insonix on 8/6/17.
 */

public interface NotificationPresenter {

    void getNotifications();

    void readNotification(String notiId);
}
