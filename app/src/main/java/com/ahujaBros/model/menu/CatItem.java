package com.ahujaBros.model.menu;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CatItem{

	@SerializedName("image")
	private String image;

	@SerializedName("SubCat")
	private List<SubCatItem> subCat;

	@SerializedName("name")
	private String name;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setSubCat(List<SubCatItem> subCat){
		this.subCat = subCat;
	}

	public List<SubCatItem> getSubCat(){
		return subCat;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"CatItem{" + 
			"image = '" + image + '\'' + 
			",subCat = '" + subCat + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}