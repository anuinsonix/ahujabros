package com.ahujaBros.model.menu;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SubCatItem implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("department_id")
	private String departmentId;

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("name")
	private String name;

	@SerializedName("sub_group_id")
	private String subGroupId;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setDepartmentId(String departmentId){
		this.departmentId = departmentId;
	}

	public String getDepartmentId(){
		return departmentId;
	}

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setSubGroupId(String subGroupId){
		this.subGroupId = subGroupId;
	}

	public String getSubGroupId(){
		return subGroupId;
	}

	@Override
 	public String toString(){
		return 
			"SubCatItem{" + 
			"image = '" + image + '\'' + 
			",department_id = '" + departmentId + '\'' + 
			",group_id = '" + groupId + '\'' + 
			",name = '" + name + '\'' + 
			",sub_group_id = '" + subGroupId + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.image);
		dest.writeString(this.departmentId);
		dest.writeString(this.groupId);
		dest.writeString(this.name);
		dest.writeString(this.subGroupId);
	}

	public SubCatItem() {
	}

	protected SubCatItem(Parcel in) {
		this.image = in.readString();
		this.departmentId = in.readString();
		this.groupId = in.readString();
		this.name = in.readString();
		this.subGroupId = in.readString();
	}

	public static final Parcelable.Creator<SubCatItem> CREATOR = new Parcelable.Creator<SubCatItem>() {
		@Override
		public SubCatItem createFromParcel(Parcel source) {
			return new SubCatItem(source);
		}

		@Override
		public SubCatItem[] newArray(int size) {
			return new SubCatItem[size];
		}
	};
}