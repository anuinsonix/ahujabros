package com.ahujaBros.model.order;

/**
 * Created by insonix on 14/6/17.
 */


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductInfo implements Serializable {

    @SerializedName("basic_rate")
    private String mBasicRate;
    @SerializedName("brand_name")
    private String mBrandName;
    @SerializedName("color")
    private String mColor;
    @SerializedName("department_id")
    private String mDepartmentId;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("ERN_no")
    private String mERNNo;
    @SerializedName("id")
    private String mId;
    @SerializedName("MRP")
    private String mMRP;
    @SerializedName("other_image_1")
    private String mOtherImage1;
    @SerializedName("other_image_2")
    private String mOtherImage2;
    @SerializedName("other_image_3")
    private String mOtherImage3;
    @SerializedName("other_image_4")
    private String mOtherImage4;
    @SerializedName("parent_category_id")
    private String mParentCategoryId;
    @SerializedName("product_image")
    private String mProductImage;
    @SerializedName("purchase_rate")
    private String mPurchaseRate;
    @SerializedName("qty")
    private String mQty;
    @SerializedName("size")
    private String mSize;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("style_description")
    private String mStyleDescription;
    @SerializedName("style_no")
    private String mStyleNo;
    @SerializedName("sub_category_id")
    private String mSubCategoryId;
    @SerializedName("supplier_name")
    private String mSupplierName;
    @SerializedName("total_qty")
    private String mTotalQuantity;
    @SerializedName("bag_id")
    private String mBagId;
    List imageList;

    public List getImageList() {
        return imageList;
    }

    public void setImageList(List imageList) {
        this.imageList = imageList;
    }

    public String getmBagId() {
        return mBagId;
    }

    public void setmBagId(String mBagId) {
        this.mBagId = mBagId;
    }

    public String getmTotalQuantity() {
        return mTotalQuantity;
    }

    public void setmTotalQuantity(String mTotalQuantity) {
        this.mTotalQuantity = mTotalQuantity;
    }

    public String getBasicRate() {
        return mBasicRate;
    }

    public void setBasicRate(String basicRate) {
        mBasicRate = basicRate;
    }

    public String getBrandName() {
        return mBrandName;
    }

    public void setBrandName(String brandName) {
        mBrandName = brandName;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getDepartmentId() {
        return mDepartmentId;
    }

    public void setDepartmentId(String departmentId) {
        mDepartmentId = departmentId;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getERNNo() {
        return mERNNo;
    }

    public void setERNNo(String ERNNo) {
        mERNNo = ERNNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMRP() {
        return mMRP;
    }

    public void setMRP(String MRP) {
        mMRP = MRP;
    }

    public String getOtherImage1() {
        return mOtherImage1;
    }

    public void setOtherImage1(String otherImage1) {
        mOtherImage1 = otherImage1;
    }

    public String getOtherImage2() {
        return mOtherImage2;
    }

    public void setOtherImage2(String otherImage2) {
        mOtherImage2 = otherImage2;
    }

    public String getOtherImage3() {
        return mOtherImage3;
    }

    public void setOtherImage3(String otherImage3) {
        mOtherImage3 = otherImage3;
    }

    public String getOtherImage4() {
        return mOtherImage4;
    }

    public void setOtherImage4(String otherImage4) {
        mOtherImage4 = otherImage4;
    }

    public String getParentCategoryId() {
        return mParentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        mParentCategoryId = parentCategoryId;
    }

    public String getProductImage() {
        return mProductImage;
    }

    public void setProductImage(String productImage) {
        mProductImage = productImage;
    }

    public String getPurchaseRate() {
        return mPurchaseRate;
    }

    public void setPurchaseRate(String purchaseRate) {
        mPurchaseRate = purchaseRate;
    }

    public String getQty() {
        return mQty;
    }

    public void setQty(String qty) {
        mQty = qty;
    }

    public String getSize() {
        return mSize;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStyleDescription() {
        return mStyleDescription;
    }

    public void setStyleDescription(String styleDescription) {
        mStyleDescription = styleDescription;
    }

    public String getStyleNo() {
        return mStyleNo;
    }

    public void setStyleNo(String styleNo) {
        mStyleNo = styleNo;
    }

    public String getSubCategoryId() {
        return mSubCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        mSubCategoryId = subCategoryId;
    }

    public String getSupplierName() {
        return mSupplierName;
    }

    public void setSupplierName(String supplierName) {
        mSupplierName = supplierName;
    }
}

