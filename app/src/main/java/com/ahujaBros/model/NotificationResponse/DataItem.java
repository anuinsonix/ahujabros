package com.ahujaBros.model.NotificationResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("msg")
	private String msg;

	@SerializedName("filename")
	private String filename;

	@SerializedName("subject")
	private String subject;

	@SerializedName("id")
	private String id;

	@SerializedName("sent-on")
	private String sentOn;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setFilename(String filename){
		this.filename = filename;
	}

	public String getFilename(){
		return filename;
	}

	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getSubject(){
		return subject;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSentOn(String sentOn){
		this.sentOn = sentOn;
	}

	public String getSentOn(){
		return sentOn;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"msg = '" + msg + '\'' + 
			",filename = '" + filename + '\'' + 
			",subject = '" + subject + '\'' + 
			",id = '" + id + '\'' + 
			",sent-on = '" + sentOn + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}