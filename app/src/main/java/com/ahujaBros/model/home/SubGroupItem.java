package com.ahujaBros.model.home;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SubGroupItem{

	@SerializedName("sub_group_id")
	private String subGroupId;

	@SerializedName("sub_group_name")
	private String subGroupName;

	public void setSubGroupId(String subGroupId){
		this.subGroupId = subGroupId;
	}

	public String getSubGroupId(){
		return subGroupId;
	}

	public void setSubGroupName(String subGroupName){
		this.subGroupName = subGroupName;
	}

	public String getSubGroupName(){
		return subGroupName;
	}

	@Override
 	public String toString(){
		return 
			"SubGroupItem{" + 
			"sub_group_id = '" + subGroupId + '\'' + 
			",sub_group_name = '" + subGroupName + '\'' + 
			"}";
		}
}