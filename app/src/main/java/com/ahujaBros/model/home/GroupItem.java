package com.ahujaBros.model.home;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GroupItem{

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("group_name")
	private String groupName;

	@SerializedName("sub_group")
	private List<SubGroupItem> subGroup;

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	public void setGroupName(String groupName){
		this.groupName = groupName;
	}

	public String getGroupName(){
		return groupName;
	}

	public void setSubGroup(List<SubGroupItem> subGroup){
		this.subGroup = subGroup;
	}

	public List<SubGroupItem> getSubGroup(){
		return subGroup;
	}

	@Override
 	public String toString(){
		return 
			"GroupItem{" + 
			"group_id = '" + groupId + '\'' + 
			",group_name = '" + groupName + '\'' + 
			",sub_group = '" + subGroup + '\'' + 
			"}";
		}
}