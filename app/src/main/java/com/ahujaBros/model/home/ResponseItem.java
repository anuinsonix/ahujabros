package com.ahujaBros.model.home;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseItem{

	@SerializedName("category_image")
	private List<String> categoryImage;

	@SerializedName("category_name")
	private String categoryName;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("group")
	private List<GroupItem> group;

	public void setCategoryImage(List<String> categoryImage){
		this.categoryImage = categoryImage;
	}

	public List<String> getCategoryImage(){
		return categoryImage;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setGroup(List<GroupItem> group){
		this.group = group;
	}

	public List<GroupItem> getGroup(){
		return group;
	}

	@Override
 	public String toString(){
		return 
			"ResponseItem{" + 
			"category_image = '" + categoryImage + '\'' + 
			",category_name = '" + categoryName + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",group = '" + group + '\'' + 
			"}";
		}
}