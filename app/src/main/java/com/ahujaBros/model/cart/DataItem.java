package com.ahujaBros.model.cart;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("color")
	private String color;

	@SerializedName("department_id")
	private String departmentId;

	@SerializedName("total_qty")
	private String totalQty;

	@SerializedName("purchase_rate")
	private int purchaseRate;

	@SerializedName("discount")
	private String discount;

	@SerializedName("MRP")
	private int mRP;

	@SerializedName("style_description")
	private String styleDescription;

	@SerializedName("bag_id")
	private String bagId;

	@SerializedName("parent_category_id")
	private String parentCategoryId;

	@SerializedName("basic_rate")
	private int basicRate;

	@SerializedName("style_number")
	private String styleNumber;

	@SerializedName("size")
	private String size;

	@SerializedName("brand_name")
	private String brandName;

	@SerializedName("supplier_name")
	private String supplierName;

	@SerializedName("Image_url")
	private String imageUrl;

	@SerializedName("sub_category_id")
	private String subCategoryId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("brand")
	private String brand;

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setDepartmentId(String departmentId){
		this.departmentId = departmentId;
	}

	public String getDepartmentId(){
		return departmentId;
	}

	public void setTotalQty(String totalQty){
		this.totalQty = totalQty;
	}

	public String getTotalQty(){
		return totalQty;
	}

	public void setPurchaseRate(int purchaseRate){
		this.purchaseRate = purchaseRate;
	}

	public int getPurchaseRate(){
		return purchaseRate;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public void setMRP(int mRP){
		this.mRP = mRP;
	}

	public int getMRP(){
		return mRP;
	}

	public void setStyleDescription(String styleDescription){
		this.styleDescription = styleDescription;
	}

	public String getStyleDescription(){
		return styleDescription;
	}

	public void setBagId(String bagId){
		this.bagId = bagId;
	}

	public String getBagId(){
		return bagId;
	}

	public void setParentCategoryId(String parentCategoryId){
		this.parentCategoryId = parentCategoryId;
	}

	public String getParentCategoryId(){
		return parentCategoryId;
	}

	public void setBasicRate(int basicRate){
		this.basicRate = basicRate;
	}

	public int getBasicRate(){
		return basicRate;
	}

	public void setStyleNumber(String styleNumber){
		this.styleNumber = styleNumber;
	}

	public String getStyleNumber(){
		return styleNumber;
	}

	public void setSize(String size){
		this.size = size;
	}

	public String getSize(){
		return size;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setSubCategoryId(String subCategoryId){
		this.subCategoryId = subCategoryId;
	}

	public String getSubCategoryId(){
		return subCategoryId;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"color = '" + color + '\'' + 
			",department_id = '" + departmentId + '\'' + 
			",total_qty = '" + totalQty + '\'' + 
			",purchase_rate = '" + purchaseRate + '\'' + 
			",discount = '" + discount + '\'' + 
			",mRP = '" + mRP + '\'' + 
			",style_description = '" + styleDescription + '\'' + 
			",bag_id = '" + bagId + '\'' + 
			",parent_category_id = '" + parentCategoryId + '\'' + 
			",basic_rate = '" + basicRate + '\'' + 
			",style_number = '" + styleNumber + '\'' + 
			",size = '" + size + '\'' + 
			",image_url = '" + imageUrl + '\'' + 
			",sub_category_id = '" + subCategoryId + '\'' + 
			",product_id = '" + productId + '\'' + 
			",qty = '" + qty + '\'' + 
			",brand = '" + brand + '\'' + 
			"}";
		}

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
}