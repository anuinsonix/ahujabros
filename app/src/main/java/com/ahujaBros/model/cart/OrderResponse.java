package com.ahujaBros.model.cart;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderResponse{

	@SerializedName("message")
	private String message;

	@SerializedName("order_id")
	private int orderId;

	@SerializedName("status")
	private String status;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setOrderId(int orderId){
		this.orderId = orderId;
	}

	public int getOrderId(){
		return orderId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderResponse{" + 
			"message = '" + message + '\'' + 
			",order_id = '" + orderId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}