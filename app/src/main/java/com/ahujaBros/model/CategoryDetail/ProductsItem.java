package com.ahujaBros.model.CategoryDetail;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ProductsItem{

	@SerializedName("color")
	private String color;

	@SerializedName("department_id")
	private String departmentId;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("purchase_rate")
	private String purchaseRate;

	@SerializedName("discount")
	private String discount;

	@SerializedName("brand_name")
	private String brandName;

	@SerializedName("MRP")
	private String mRP;

	@SerializedName("style_description")
	private String styleDescription;

	@SerializedName("basic_rate")
	private String basicRate;

	@SerializedName("parent_category_id")
	private String parentCategoryId;

	@SerializedName("size")
	private String size;

	@SerializedName("created_on")
	private String createdOn;

	@SerializedName("sub_category_id")
	private String subCategoryId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("ERN_no")
	private String eRNNo;

	@SerializedName("style_no")
	private String styleNo;

	@SerializedName("id")
	private String id;

	@SerializedName("supplier_name")
	private String supplierName;

	@SerializedName("status")
	private String status;

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setDepartmentId(String departmentId){
		this.departmentId = departmentId;
	}

	public String getDepartmentId(){
		return departmentId;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setPurchaseRate(String purchaseRate){
		this.purchaseRate = purchaseRate;
	}

	public String getPurchaseRate(){
		return purchaseRate;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public void setBrandName(String brandName){
		this.brandName = brandName;
	}

	public String getBrandName(){
		return brandName;
	}

	public void setMRP(String mRP){
		this.mRP = mRP;
	}

	public String getMRP(){
		return mRP;
	}

	public void setStyleDescription(String styleDescription){
		this.styleDescription = styleDescription;
	}

	public String getStyleDescription(){
		return styleDescription;
	}

	public void setBasicRate(String basicRate){
		this.basicRate = basicRate;
	}

	public String getBasicRate(){
		return basicRate;
	}

	public void setParentCategoryId(String parentCategoryId){
		this.parentCategoryId = parentCategoryId;
	}

	public String getParentCategoryId(){
		return parentCategoryId;
	}

	public void setSize(String size){
		this.size = size;
	}

	public String getSize(){
		return size;
	}

	public void setCreatedOn(String createdOn){
		this.createdOn = createdOn;
	}

	public String getCreatedOn(){
		return createdOn;
	}

	public void setSubCategoryId(String subCategoryId){
		this.subCategoryId = subCategoryId;
	}

	public String getSubCategoryId(){
		return subCategoryId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setERNNo(String eRNNo){
		this.eRNNo = eRNNo;
	}

	public String getERNNo(){
		return eRNNo;
	}

	public void setStyleNo(String styleNo){
		this.styleNo = styleNo;
	}

	public String getStyleNo(){
		return styleNo;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSupplierName(String supplierName){
		this.supplierName = supplierName;
	}

	public String getSupplierName(){
		return supplierName;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SearchProductsItem{" +
			"color = '" + color + '\'' + 
			",department_id = '" + departmentId + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",purchase_rate = '" + purchaseRate + '\'' + 
			",discount = '" + discount + '\'' + 
			",brand_name = '" + brandName + '\'' + 
			",mRP = '" + mRP + '\'' + 
			",style_description = '" + styleDescription + '\'' + 
			",basic_rate = '" + basicRate + '\'' + 
			",parent_category_id = '" + parentCategoryId + '\'' + 
			",size = '" + size + '\'' + 
			",created_on = '" + createdOn + '\'' + 
			",sub_category_id = '" + subCategoryId + '\'' + 
			",qty = '" + qty + '\'' + 
			",eRN_no = '" + eRNNo + '\'' + 
			",style_no = '" + styleNo + '\'' + 
			",id = '" + id + '\'' + 
			",supplier_name = '" + supplierName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}