package com.ahujaBros.ui;

/**
 * Created by insonix on 19/5/17.
 */

public interface IndicatorHandler {

    void handleVisibility(boolean visible);

}
