package com.ahujaBros.ui.base;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.NetworkUtil;
import com.google.common.eventbus.Subscribe;
import com.src.ahujaBros.R;

/**
 * Created by insonix on 23/5/17.
 */

public class BaseFragment extends Fragment implements BaseView{


    private Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (NetworkUtil.getConnectivityStatus(getActivity()) == 0) {
            CommonUtils.showNetworkUnavailable(getActivity());
            return;
        }
        dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent);
        View views = LayoutInflater.from(getActivity()).inflate(R.layout.dot_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(views);
    }

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void showProgress() {
        if (NetworkUtil.getConnectivityStatus(getActivity()) != 0) {
            dialog.show();
        } else {
            CommonUtils.showNetworkUnavailable(getActivity());
        }
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showError(String str) {
        Toast.makeText(getActivity(),str,Toast.LENGTH_LONG).show();
    }
}
