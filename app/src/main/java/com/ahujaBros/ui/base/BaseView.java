package com.ahujaBros.ui.base;

/**
 * Created by insonix on 29/5/17.
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showError(String s);

}
