package com.ahujaBros.ui;

import com.ahujaBros.model.menu.SubCatItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insonix on 17/5/17.
 */

public interface NavigationManager {

    void showFragmentHome(String title, String id);

    void showFragmentCategory(ArrayList<SubCatItem> listSub, String title, String selectedGroupID, String selectedChildId, String depttID);

    void showFragmentContactUs();

//    void showFragmentDrama(String title);
//
//    void showFragmentMusical(String title);
//
//    void showFragmentThriller(String title);
}
