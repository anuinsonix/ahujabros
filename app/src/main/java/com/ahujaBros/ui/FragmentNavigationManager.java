package com.ahujaBros.ui;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.ahujaBros.model.menu.SubCatItem;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.fragment.CategoryDetailFragment;
import com.ahujaBros.ui.fragment.FragmentContactUs;
import com.ahujaBros.ui.fragment.HomeFragment;
import com.src.ahujaBros.BuildConfig;
import com.src.ahujaBros.R;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * @author Insonix
 */

public class FragmentNavigationManager implements NavigationManager {

    private static FragmentNavigationManager sInstance;

    private FragmentManager mFragmentManager;
    private MainActivity mActivity;

    public static FragmentNavigationManager obtain(MainActivity activity) {
        if (sInstance == null) {
            sInstance = new FragmentNavigationManager();
        }
        sInstance.configure(activity);
        return sInstance;
    }

    private void configure(MainActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }

    @Override
    public void showFragmentHome(String title, String id) {
        showFragment(HomeFragment.newInstance(title,id), false);
    }

    @Override
    public void showFragmentCategory(ArrayList<SubCatItem> list, String title, String selectedGroupID, String selectedChildId, String depttID) {
        showFragment(CategoryDetailFragment.newInstance(list,title,selectedGroupID,selectedChildId,depttID),false);
    }

    @Override
    public void showFragmentContactUs() {
        showFragment(new FragmentContactUs(),false);
    }

    public void showFragment(Fragment fragment, boolean allowStateLoss) {
        FragmentManager fm = mFragmentManager;
        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction()
                .add(R.id.container, fragment);
        Log.i(TAG, "showFragment: "+fragment.getId());
        ft.addToBackStack(""+fragment.getId());

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }
}
