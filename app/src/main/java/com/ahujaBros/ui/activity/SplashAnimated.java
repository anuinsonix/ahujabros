package com.ahujaBros.ui.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;

import com.ahujaBros.MyApplication;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.NetworkUtil;
import com.src.ahujaBros.R;


/**
 * Created by insonix on 23/5/17.
 */

public class SplashAnimated extends AppCompatActivity {

    Transition returnTrans;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            returnTrans = new Slide();
            returnTrans.setDuration(3000);
            ((Slide) returnTrans).setSlideEdge(Gravity.LEFT);
            getWindow().setEnterTransition(returnTrans);
            getWindow().setExitTransition(null);
        }

        setContentView(R.layout.activity_splash_animated);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            returnTrans.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {
                    
                }
    
                @Override
                public void onTransitionEnd(Transition transition) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startNextActivity();

                        }
                    },2000);

                }
    
                @Override
                public void onTransitionCancel(Transition transition) {
    
                }
    
                @Override
                public void onTransitionPause(Transition transition) {
    
                }
    
                @Override
                public void onTransitionResume(Transition transition) {
    
                }
            });
        }
    }

    private void startNextActivity() {
        ActivityOptionsCompat options = null;
        if(NetworkUtil.getConnectivityStatus(SplashAnimated.this)!=0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                options = ActivityOptionsCompat .makeSceneTransitionAnimation(
                        SplashAnimated.this);
                if(MyPreferences.getInstance(MyApplication.getInstance()).getString(MyPreferences.Key.USERID)==null){
                    Intent intent = new Intent(SplashAnimated.this, LoginActivity.class);
                    ActivityCompat.startActivity(SplashAnimated.this,intent, null);
                    finish();
                }else{
                    Intent intent = new Intent(SplashAnimated.this, MainActivity.class);
                    ActivityCompat.startActivity(SplashAnimated.this,intent, null);
                    finish();
                }
            }
        }else{
            CommonUtils.showNetworkUnavailable(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        startNextActivity();
    }
}
