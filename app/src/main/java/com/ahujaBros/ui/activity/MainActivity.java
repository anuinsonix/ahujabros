package com.ahujaBros.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.ArrayAdapterSearchView;
import com.ahujaBros.adapter.CustomExpandableListAdapter;
import com.ahujaBros.model.Search.ProductFoundResponse;
import com.ahujaBros.model.generic.BadgeResponse;
import com.ahujaBros.model.menu.CatItem;
import com.ahujaBros.model.menu.MenuResponse;
import com.ahujaBros.model.menu.ResponseItem;
import com.ahujaBros.presenter.CategoriesPresenter;
import com.ahujaBros.presenter.CategoriesPresenterImpl;
import com.ahujaBros.receiver.FcmBroadcastReceiver;
import com.ahujaBros.ui.CartUpdateListener;
import com.ahujaBros.ui.FragmentNavigationManager;
import com.ahujaBros.ui.MainView;
import com.ahujaBros.ui.NavigationManager;
import com.ahujaBros.ui.base.BaseActivity;
import com.ahujaBros.ui.fragment.AddtoBagFragment;
import com.ahujaBros.ui.fragment.CategoryDetailFragment;
import com.ahujaBros.ui.fragment.FragmentContactUs;
import com.ahujaBros.ui.fragment.NotificationFragment;
import com.ahujaBros.ui.fragment.OrderHistoryFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.NotificationListener;
import com.src.ahujaBros.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ahujaBros.utils.CommonUtils.setBadgeCount;


public class MainActivity extends BaseActivity implements MainView, AdapterView.OnItemClickListener,NotificationListener, FcmBroadcastReceiver.NotificationUpdateListener,AddToolbarTitle {
    private static final String TAG = MainActivity.class.getName();
    @BindView(R.id.options)
    ListView options;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle = MainActivity.class.getName();
    private ExpandableListView mExpandableListView;
    private ExpandableListAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private NavigationManager mNavigationManager;
    private Map<String, ArrayList<CatItem>> mExpandableListData;
    CategoriesPresenter mPresenter;
    private Slide returnTrans;
    LayerDrawable notiIcon;
    static LayerDrawable cartIcon;
    private Events.CartCountEvent cartCount;
    private ArrayAdapter arrayAdapter;
    ArrayAdapterSearchView searchView;
    private FcmBroadcastReceiver receiver;
    MenuItem itemCart,itemNoti;
    private boolean doubleBackToExitPressedOnce;
    public static CartUpdateListener cartListener;
    public static boolean searchDone;

    @Subscribe(threadMode = ThreadMode.MAIN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enter transition
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            returnTrans = new Slide();
            returnTrans.setDuration(2000);
            returnTrans.setSlideEdge(Gravity.LEFT);
//            getWindow().setEnterTransition(returnTrans);
            getWindow().setEnterTransition(null);
        }*/
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        mPresenter = new CategoriesPresenterImpl(this, this);
        mPresenter.getCategories();
        mExpandableListView = (ExpandableListView) findViewById(R.id.navList);
        mNavigationManager = FragmentNavigationManager.obtain(this);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.nav_header, null, false);
        LinearLayout ll = (LinearLayout) listHeaderView.findViewById(R.id.home_item);
        mExpandableListView.addHeaderView(listHeaderView);
        setupDrawer();
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNavigationManager.showFragmentHome("", "");
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        mPresenter.getSearchKeywords();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        getSupportActionBar().setCustomView(R.layout.title);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        setOtherOptionsMenu();
      /*  getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(getSupportFragmentManager().getBackStackEntryCount()>1)
                mDrawerToggle.setDrawerIndicatorEnabled(false);
            }
        });
*/
        receiver=new FcmBroadcastReceiver();        //initialize receiver and setting listener event
        receiver.setListener(this);
    }



//TO Fix no public methods found for @Subscribe annotation
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCartCountEvent(Events.CartCountEvent event) {
//        Toast.makeText(this, event.getCount(), Toast.LENGTH_SHORT).show();
    }

    private void selectFirstItemAsDefault(List<String> mExpandableListTitle) {
        if (mNavigationManager != null) {
            String firstActionMovie = mExpandableListTitle.get(0);
            mNavigationManager.showFragmentHome("Home", "");
            getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    @Override
    public void onBackPressed() {
//        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            if (doubleBackToExitPressedOnce) {
                Log.e("countii2", String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));
//                getFragmentManager().popBackStack();
                finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    public void addDrawerItems() {
        mExpandableListAdapter = new CustomExpandableListAdapter(this, mExpandableListTitle, mExpandableListData);
        mExpandableListView.setAdapter(mExpandableListAdapter);
        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                getSupportActionBar().setTitle(mExpandableListTitle.get(groupPosition).toString());
                mExpandableListAdapter = mExpandableListView.getExpandableListAdapter();
                if (mExpandableListAdapter == null) {
                    return;
                }
                for (int i = 0; i < mExpandableListAdapter.getGroupCount(); i++) {
                    if (i != groupPosition) {
                        mExpandableListView.collapseGroup(i);
                    }
                }
            }
        });

        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                getSupportActionBar().setTitle(R.string.app_name);
            }
        });

        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                CatItem selectedItem = mExpandableListData.get(mExpandableListTitle.get(groupPosition)).get(childPosition);
                String selectedChildId = mExpandableListData.get(mExpandableListTitle.get(groupPosition)).get(childPosition).getSubCat().get(0).getGroupId();

                getSupportActionBar().setTitle(selectedItem.getName());
                if (groupPosition == 0) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 1) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 2) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 3) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 4) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 5) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 6) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 7) {
                    mNavigationManager.showFragmentCategory(new ArrayList<>(selectedItem.getSubCat()), selectedItem.getName(), selectedItem.getSubCat().get(0).getGroupId(), selectedItem.getSubCat().get(0).getSubGroupId(), selectedItem.getSubCat().get(0).getDepartmentId());
                } else if (groupPosition == 8) {
                    mNavigationManager.showFragmentContactUs();
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void setOtherOptionsMenu() {
        String[] menuOptions = {"Order Detail", "Contact Us", "Logout"};
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<>(this, R.layout.menu_item_list, menuOptions);
        options.setAdapter(itemsAdapter);
        options.setOnItemClickListener(this);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.app_name);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(mActivityTitle);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigationManager.showFragmentHome("", "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        itemCart = menu.findItem(R.id.action_cart);
        itemNoti = menu.findItem(R.id.action_noti);
        MenuItem searchItem = menu.findItem(R.id.search);
        notiIcon = (LayerDrawable) itemNoti.getIcon();
        cartIcon = (LayerDrawable) itemCart.getIcon();
        mPresenter.getSearchKeywords();
        //Get Badge count and cart count
        mPresenter.getBadgeCount();
        searchView = (ArrayAdapterSearchView) MenuItemCompat.getActionView(searchItem);
        updateUI();             //for notification badge update
        updateCartCount();           //cart badge update
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (id) {
            case (R.id.action_cart):
                CommonUtils.addFragment(this, new AddtoBagFragment(), false, "AddtoBag", null);
//                itemCart.setIcon(R.drawable.ic_action_notification);        //to clear cart count

                break;
            case (R.id.action_noti):
                CommonUtils.addFragment(this, new NotificationFragment(), false, "Notification", null);
                itemNoti.setIcon(R.drawable.ic_action_notification);        //to clear badge count
                break;
            case (R.id.home):
                int count = getSupportFragmentManager().getBackStackEntryCount();
                if (count == 0) {
                    super.onBackPressed();
                } else {
                    getSupportFragmentManager().popBackStack();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getMenuCategories(MenuResponse categoryResponse) {
        mExpandableListData = new ArrayMap<>();
        mExpandableListTitle = new ArrayList<>();
        ArrayList<CatItem> childTitles;
        for (int i = 0; i < categoryResponse.getResponse().size(); i++) {
            ResponseItem CatTitle = categoryResponse.getResponse().get(i);
            childTitles = new ArrayList<>();
            for (int j = 0; j < categoryResponse.getResponse().get(i).getCat().size(); j++) {
                if (CatTitle.getCat().size() > 0) {
//                    childTitles.add(CatTitle.getCat().get(j).getName()+"-"+CatTitle.getCat().get(j).getSubCat().get(0).getGroupId());
                    childTitles.add(CatTitle.getCat().get(j));
                    //appended group Id for further use in same string with regex "-"

                } else {
                    childTitles = new ArrayList<>();
                }
                Log.i(TAG, "getMenuCategories: " + childTitles.size());
            }
            mExpandableListData.put(CatTitle.getDepartment(), childTitles);
        }
        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
        Collections.sort(mExpandableListTitle);
        addDrawerItems();
        selectFirstItemAsDefault(mExpandableListTitle);             //to select first item as default
    }

    /**
     * To update cart
     */
    public void updateCartCount() {
        cartCount = GlobalBus.getBus().getStickyEvent(Events.CartCountEvent.class);
        if (cartCount != null) {
            setBadgeCount(this, cartIcon, "" + cartCount.getCount(), 0);        // 0 is sent for cart
        }else{
            setBadgeCount(this,cartIcon, String.valueOf(MyPreferences.getInstance().getInt(MyPreferences.Key.CART_COUNT)),0);
        }
    }

    @Override
    public void updateBadgeCount(BadgeResponse response) {
        if (response.getCount() > 0) {
            setBadgeCount(this, notiIcon, "" + response.getCount(), 1);         // 1 is sent for cart
        }else{
            updateUI();
        }
    }

    @Override
    public void searchedData(ProductFoundResponse response) {
        GlobalBus.getBus().postSticky(new Events.CategoriesSearchedEvent(response.getData().getProducts()));
        CommonUtils.addFragment(this, new CategoryDetailFragment(), false, "Categories", null);
    }

    @Override
    public void searchKeywordsList(List<String> data) {
        arrayAdapter = new ArrayAdapter(MainActivity.this, R.layout.search_spinner_item, R.id.text, data);
        searchView.setAdapter(arrayAdapter);
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchDone=true;
                searchView.setText(arrayAdapter.getItem(position).toString());
                mPresenter.searchKeyword(arrayAdapter.getItem(position).toString().trim());

            }
        });
    }

    public static Intent createInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                CommonUtils.addFragment(this, new OrderHistoryFragment(), false, "History", null);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
            case 1:
                CommonUtils.addFragment(this, new FragmentContactUs(), false, "contact", null);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
            case 2:
                CommonUtils.logoutApp(MainActivity.this);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void getBadgeCount(int length) {
        updateBadgeCount(length);
    }

    private void updateBadgeCount(int length) {
        if (length > 0) {
            setBadgeCount(this, notiIcon, "" + length, 1);
        }
    }

    @Override
    public void getCartCount(int length) {
        updateCartCount();
    }

    @Override
    public void updateUI() {
        int count=0;
        int saveValue=MyPreferences.getInstance().getInt(MyPreferences.Key.NOTIFICATION_COUNT);
        count=saveValue+count;
        updateBadgeCount(count);
        Log.i(TAG, "updateUI: "+count);
        CommonUtils.setBadge(this,count);
    }


    @Override
    public void changeToolbarTitle(String s) {
        Log.i(TAG, "changeToolbarTitle: "+s);
        RelativeLayout titleLayout=(RelativeLayout) getSupportActionBar().getCustomView();
        TextView titleTv=(TextView)titleLayout.findViewById(R.id.titleTv);
        titleTv.setText(s);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void hideUpButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    public void showUpButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
