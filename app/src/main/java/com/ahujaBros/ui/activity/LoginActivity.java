package com.ahujaBros.ui.activity;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ahujaBros.MyApplication;
import com.ahujaBros.model.Login.LoginResponse;
import com.ahujaBros.presenter.LoginPresenter;
import com.ahujaBros.presenter.LoginPresenterImpl;
import com.ahujaBros.ui.LoginView;
import com.ahujaBros.ui.base.BaseActivity;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.src.ahujaBros.R;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 31/5/17.
 */

public class LoginActivity extends AppCompatActivity implements LoginView {


    private static final String ANDROID = "android";
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.logocontainer)
    RelativeLayout logocontainer;
    @BindView(R.id.tag_line)
    TextView tagLine;
    @BindView(R.id.editUsername)
    EditText editUsername;
    @BindView(R.id.input_layout_user)
    TextInputLayout inputLayoutUser;
    @BindView(R.id.editPasswd)
    EditText editPasswd;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.lin_na)
    LinearLayout linNa;
    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.maincontainer)
    RelativeLayout maincontainer;
    @BindView(R.id.scroll)
    ScrollView scroll;
    private boolean isLogoShown = false;
    LoginPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter = new LoginPresenterImpl(this);
        final String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname = editUsername.getText().toString().trim();
                String pwd = editPasswd.getText().toString().trim();
                if (validateUser(uname, pwd)) {
                    mPresenter.loginUser(uname, pwd, android_id, ANDROID, MyPreferences.getInstance(LoginActivity.this).getString(MyPreferences.Key.FCM_TOKEN));
                }
            }
        });
    }


    @Override
    public boolean validateUser(String user, String pwd) {
        if (user.isEmpty()) {
            CommonUtils.showErrorSnack(getWindow(), R.string.empty_username);
            return false;
        } else if (pwd.isEmpty()) {
            CommonUtils.showErrorSnack(getWindow(), R.string.empty_pwd);
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessLogin(LoginResponse loginResponse) {
        if(loginResponse.getStatus().equalsIgnoreCase("true")){
            CommonUtils.showErrorSnack(getWindow(), R.string.success_login);
            MyPreferences.getInstance(MyApplication.getInstance()).put(MyPreferences.Key.USERID, loginResponse.getId());
            startActivity(MainActivity.createInstance(this));
            finish();
        }else{
            CommonUtils.showActivitySnack(getWindow(),loginResponse.getMessage());
        }

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String s) {
        Snackbar.make(getWindow().getDecorView(),s,Snackbar.LENGTH_LONG).show();
    }
}
