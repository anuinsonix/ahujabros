package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahujaBros.adapter.NotificationAdapter;
import com.ahujaBros.model.NotificationResponse.DataItem;
import com.ahujaBros.model.NotificationResponse.NotificationResponse;
import com.ahujaBros.presenter.NotificationPresenter;
import com.ahujaBros.presenter.NotificationPresenterImpl;
import com.ahujaBros.ui.NotificationView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.src.ahujaBros.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 8/6/17.
 */

public class NotificationFragment extends BaseFragment implements NotificationView, RecyclerItemGenricClickListener {


    @BindView(R.id.noti_list)
    RecyclerView notiList;
    Unbinder unbinder;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    private NotificationAdapter adapter;
    NotificationPresenter mPresenter;
    private ArrayList<DataItem> mNotificationList;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener= ((MainActivity)getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        mTitleListener.changeToolbarTitle(getResources().getString(R.string.notifications));
        initRecyclerView();
        mPresenter = new NotificationPresenterImpl(this);
        mPresenter.getNotifications();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(false);
                mPresenter.getNotifications();
            }
        });
        return view;
    }

    private void initRecyclerView() {
        adapter = new NotificationAdapter(new ArrayList<DataItem>(), this);
        notiList.setLayoutManager(new LinearLayoutManager(getActivity()));
        notiList.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLoadingNotifications(NotificationResponse response) {
        adapter.refresh(response.getData());
        mNotificationList = new ArrayList<>(response.getData());
        MyPreferences.getInstance(getActivity()).put(MyPreferences.Key.NOTIFICATION_COUNT, getUnreadNotifications(mNotificationList));
    }


    //To save count of unread notifications for badge count
    private int getUnreadNotifications(ArrayList<DataItem> mNotificationList) {
        int count = 0;
        for (DataItem item : mNotificationList) {
            if (item.getStatus().equalsIgnoreCase("0")) {
                count++;
            }
        }
        Log.e(TAG, "getUnreadNotifications: " + count);
        return count;
    }


    @Override
    public void onItemClick(int pos) {
        mPresenter.readNotification(mNotificationList.get(pos).getId());
        mPresenter.getNotifications();
    }
}
