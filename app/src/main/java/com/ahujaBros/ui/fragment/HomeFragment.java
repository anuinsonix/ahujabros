package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.TestAdapter;
import com.ahujaBros.model.home.GroupItem;
import com.ahujaBros.model.home.ResponseItem;
import com.ahujaBros.model.home.SubGroupItem;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.HomeItemClickListener;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.google.common.eventbus.Subscribe;
import com.ahujaBros.adapter.HomeFoldAdapter;
import com.ahujaBros.model.home.HomeResponse;
import com.ahujaBros.presenter.HomePresenter;
import com.ahujaBros.presenter.HomePresenterImpl;
import com.ahujaBros.ui.HomeView;
import com.ahujaBros.ui.base.BaseFragment;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.ContentValues.TAG;
import static com.ahujaBros.utils.Constants.KEY_SUBCATID;
import static com.ahujaBros.utils.Constants.KEY_TITLE;

/**
 * Created by insonix on 23/5/17.
 */

public class HomeFragment extends BaseFragment implements HomeView, RecyclerItemGenricClickListener, HomeItemClickListener, FragmentManager.OnBackStackChangedListener {

    @BindView(R.id.home_list)
    RecyclerView homeList;
    Unbinder unbinder;
    HomePresenter mPresenter;
    HomeFoldAdapter adapter;
    private LayoutAnimationController controller;
    private HomeResponse copyResponse;
    private List<GroupItem> groupItem;
    private ArrayList<ResponseItem> dataList;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener= ((MainActivity)getActivity());
    }


    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        mTitleListener.changeToolbarTitle(getResources().getString(R.string.app_name));
        mPresenter = new HomePresenterImpl(this);
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(this);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        dataList= new ArrayList<>();
        adapter = new HomeFoldAdapter(dataList, getActivity(), this);
        homeList.setLayoutManager(new LinearLayoutManager(getActivity()));
        homeList.setLayoutAnimation(initAnim());
        homeList.setAdapter(adapter);
    }

    private LayoutAnimationController initAnim() {
        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        controller = new LayoutAnimationController(set, 0.5f);
        return controller;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        clearBackStack();
        mPresenter.getHomeData();
    }

    private void clearBackStack() {
        FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack("Home", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void refreshData(HomeResponse response) {
        copyResponse = response;
        adapter.refreshData(response.getResponse());
    }

    public static Fragment newInstance(String title, String subCatID) {
        HomeFragment fragmentHome = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putString(KEY_SUBCATID, subCatID);
        fragmentHome.setArguments(args);
        return fragmentHome;
    }

    @Override
    public void onItemClick(int pos) {
        CommonUtils.shareItems(getActivity());
    }

    @Override
    public void onHomeListItemClick(int parentPos, int pos) {
        Log.i(TAG, "onHomeListItemClick: new"+pos);
        groupItem = copyResponse.getResponse().get(parentPos).getGroup();
        if (TestAdapter.catDial.isShowing())                         //To dismiss the dialog shown
            TestAdapter.catDial.dismiss();
        if(copyResponse.getResponse().get(parentPos).getGroup().size()>0){
            GlobalBus.getBus().postSticky(new Events.CategoryEvent(groupItem, groupItem.get(pos).getGroupName(), groupItem.get(pos).getGroupId(), groupItem.get(pos).getSubGroup().get(0).getSubGroupId(), copyResponse.getResponse().get(parentPos).getCategoryId(), (ArrayList<SubGroupItem>) copyResponse.getResponse().get(parentPos).getGroup().get(pos).getSubGroup()));                    //TO send list for tabs titles
        }else{
//            GlobalBus.getBus().postSticky(new Events.CategoryEvent(groupItem, groupItem.get(pos).getGroupName(), groupItem.get(pos).getGroupId(), groupItem.get(pos).getSubGroup().get(0).getSubGroupId(), copyResponse.getResponse().get(pos).getCategoryId(), (ArrayList<SubGroupItem>) copyResponse.getResponse().get(pos).getGroup().get(pos).getSubGroup()));                    //TO send list for tabs titles
            Log.i(TAG, "onHomeListItemClick: No more items available");
            CommonUtils.showActivitySnack(getActivity().getWindow(),"No items available");
        }
        CommonUtils.addFragment(getActivity(), new CategoryDetailFragment(), false, "Category", null);
    }

    @Override
    public void onBackStackChanged() {
       /* if(getActivity().getSupportFragmentManager().getBackStackEntryCount()<1){
            ((MainActivity)getActivity()).hideUpButton();
        }*/

    }
}
