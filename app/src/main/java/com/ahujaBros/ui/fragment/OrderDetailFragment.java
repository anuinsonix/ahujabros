package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.OrderDetailAdapter;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.FullLengthListView;
import com.src.ahujaBros.R;

/**
 * Created by root on 27/4/17.
 */

public class OrderDetailFragment extends BaseFragment {


    private View mView;
    FullLengthListView lengthListView;
    TextView textNoData;
    private Events.OrderDetailEvent mOrderDetail;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener= ((MainActivity)getActivity());
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_order_deatil, container, false);
        mTitleListener.changeToolbarTitle(getString(R.string.order_detail));
        initUI();
        return mView;
    }

    private void initUI() {
        lengthListView = (FullLengthListView) mView.findViewById(R.id.order_items_list);
        textNoData = (TextView) mView.findViewById(R.id.text_nodata);
        mOrderDetail=GlobalBus.getBus().getStickyEvent(Events.OrderDetailEvent.class);
        OrderDetailAdapter orderDetailAdapter = new OrderDetailAdapter(getActivity(), mOrderDetail.getProductList());
        lengthListView.setAdapter(orderDetailAdapter);
    }
}
