package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.ItemDetailAdapter;
import com.ahujaBros.presenter.ItemDetailPresenter;
import com.ahujaBros.presenter.ItemDetailPresenterImpl;
import com.ahujaBros.ui.ItemDetailView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.src.ahujaBros.R;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.himanshusoni.quantityview.QuantityView;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by insonix on 26/5/17.
 */

public class InItemDetailFragment extends BaseFragment implements QuantityView.OnQuantityChangeListener, ItemDetailView {


    private static final String TAG = MainActivity.class.getName();
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.scroll)
    ScrollView scroll;
    Unbinder unbinder;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.parent_layout)
    LinearLayout parentLayout;
    @BindView(R.id.price_txt)
    TextView priceTxt;
    @BindView(R.id.setprice_txt)
    TextView setpriceTxt;
    @BindView(R.id.quantityView_default)
    QuantityView quantityViewDefault;
    @BindView(R.id.buy)
    LinearLayout buy;
    @BindView(R.id.desctxt)
    TextView desctxt;
    @BindView(R.id.descLayout)
    LinearLayout descLayout;
    @BindView(R.id.title_txt)
    TextView titleTxt;
    @BindView(R.id.by_designer)
    TextView supplier;
    @BindView(R.id.desc_header)
    TextView descHeader;
    @BindView(R.id.finalprice)
    TextView finalprice;
    @BindView(R.id.buy_textbttn)
    TextView buyTextbttn;
    int total;
    private Events.CategoryToItemDetailEvent categoryItemEvent;
    ItemDetailPresenter mPresenter;
    private Events.CartItemEvent cartItem;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener = ((MainActivity) getActivity());
    }


    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_in_item_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        ((MainActivity)getActivity()).showUpButton();
        if (GlobalBus.getBus().getStickyEvent(Events.CategoryToItemDetailEvent.class) != null) {
            categoryItemEvent = GlobalBus.getBus().getStickyEvent(Events.CategoryToItemDetailEvent.class);
            mTitleListener.changeToolbarTitle(categoryItemEvent.getTitle());
            Log.i(TAG, "onCreateView: " + categoryItemEvent.getTitle());
        } else {
            mTitleListener.changeToolbarTitle(getResources().getString(R.string.app_name));
            cartItem = GlobalBus.getBus().getStickyEvent(Events.CartItemEvent.class);
            Log.i(TAG, "onCreateView: " + cartItem.getItem().getBrandName());
        }

        initViewAdapter();
        setData();
        descHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (desctxt.getVisibility() == View.VISIBLE) {
                    descHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove_black_24dp, 0);
                    desctxt.setVisibility(View.GONE);
                } else {
                    descHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove_black_24dp, 0);
                    desctxt.setVisibility(View.VISIBLE);
                }
            }
        });
        quantityViewDefault.setMinQuantity(1);
        quantityViewDefault.setOnQuantityChangeListener(this);
        quantityViewDefault.setQuantityClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText et = (EditText) v.findViewById(me.himanshusoni.quantityview.R.id.qv_et_change_qty);
                et.setText(String.valueOf(quantityViewDefault.getQuantity()));
                Log.i(TAG, "onClick: dialog");
            }
        });
        mPresenter = new ItemDetailPresenterImpl(this);
        return view;
    }

    private void setData() {

        if (GlobalBus.getBus().getStickyEvent(Events.CartItemEvent.class) != null) {
            Log.i(TAG, "setData: " + cartItem.getItem().getBrand());
            titleTxt.setText(cartItem.getItem().getBrand());
            supplier.setText("By: " + cartItem.getItem().getSupplierName());
            quantityViewDefault.setQuantity(Integer.parseInt(cartItem.getItem().getTotalQty()));
            priceTxt.setText("Price: " + getResources().getString(R.string.Rs) + cartItem.getItem().getMRP());
            setpriceTxt.setText("Set Price: " + getResources().getString(R.string.Rs) + cartItem.getItem().getBasicRate() + " " + cartItem.getItem().getPurchaseRate());
            desctxt.setText(cartItem.getItem().getStyleDescription());
            finalprice.setText("Buy Set for "+getResources().getString(R.string.Rs) + cartItem.getItem().getBasicRate());
        } else {
            titleTxt.setText(categoryItemEvent.getmItem().getBrandName());
            supplier.setText("By: " + categoryItemEvent.getmItem().getSupplierName());
            priceTxt.setText("Price: " + getResources().getString(R.string.Rs) + categoryItemEvent.getmItem().getMRP());
            setpriceTxt.setText("Set Price: " + getResources().getString(R.string.Rs) + categoryItemEvent.getmItem().getBasicRate() + " " + categoryItemEvent.getmItem().getPurchaseRate());
            desctxt.setText(categoryItemEvent.getmItem().getStyleDescription());
            finalprice.setText("Buy Set for "+getResources().getString(R.string.Rs) + categoryItemEvent.getmItem().getBasicRate());
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.search).setVisible(false);
    }

    private void initViewAdapter() {
        if (categoryItemEvent != null) {

            viewPager.setAdapter(new ItemDetailAdapter(getActivity(), categoryItemEvent.getmItem(), null));
        } else {
            viewPager.setAdapter(new ItemDetailAdapter(getActivity(), null, cartItem.getItem()));
        }
        indicator.setViewPager(viewPager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static Fragment getInstance() {
        InItemDetailFragment fragment = new InItemDetailFragment();
        return fragment;
    }

    @Override
    public void onQuantityChanged(int oldQuantity, int newQuantity, boolean programmatically) {
        if (GlobalBus.getBus().getStickyEvent(Events.CartItemEvent.class) != null) {
            total = newQuantity * cartItem.getItem().getBasicRate();
            finalprice.setText(getResources().getString(R.string.Rs) + total + " " + cartItem.getItem().getSize());
        } else {
            total = newQuantity * Integer.parseInt(categoryItemEvent.getmItem().getBasicRate());
            finalprice.setText(getResources().getString(R.string.Rs) + total + " " + categoryItemEvent.getmItem().getSize());
        }

    }

    @Override
    public void onLimitReached() {

    }

    @OnClick(R.id.finalprice)
    public void onViewClicked() {
        Log.i(TAG, "onViewClicked: " + quantityViewDefault.getQuantity());
        GlobalBus.getBus().postSticky(new Events.SelectedItemDetailEvent(categoryItemEvent.getmItem(), String.valueOf(quantityViewDefault.getQuantity())));
        CommonUtils.addFragment(getActivity(), new PaymentCartScreen(), false, "InItemDetail", null);
    }

    @Override
    public void onAddtoCartSuccess(String s) {
        CommonUtils.showActivitySnack(getActivity().getWindow(), s);
        CommonUtils.addFragment(getActivity(), new AddtoBagFragment(), false, "AddtoBagFragment", null);
    }

    @OnClick(R.id.buy_textbttn)
    public void onAddtoCartViewClicked() {
        if (cartItem != null) {

            mPresenter.addToBag(cartItem.getItem().getProductId(), String.valueOf(quantityViewDefault.getQuantity()));
        } else {

            mPresenter.addToBag(categoryItemEvent.getmItem().getId(), String.valueOf(quantityViewDefault.getQuantity()));
        }
    }
}
