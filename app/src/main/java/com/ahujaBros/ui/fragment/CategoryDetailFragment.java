package com.ahujaBros.ui.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.MyApplication;
import com.ahujaBros.adapter.CategoryAdapter;
import com.ahujaBros.adapter.SortListAdapter;
import com.ahujaBros.model.CategoryDetail.CategoryDetailResponse;
import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.ahujaBros.model.home.SubGroupItem;
import com.ahujaBros.model.menu.SubCatItem;
import com.ahujaBros.presenter.CategoryDetailPresenter;
import com.ahujaBros.presenter.CategoryDetailPresenterImpl;
import com.ahujaBros.ui.CategoryView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.AddtoCartItemGenricClickListener;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.SortItemClickListener;
import com.src.ahujaBros.R;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.ahujaBros.utils.Constants.KEY_CATID;
import static com.ahujaBros.utils.Constants.KEY_DEPTTID;
import static com.ahujaBros.utils.Constants.KEY_FRAGTITLE;
import static com.ahujaBros.utils.Constants.KEY_SUBCATID;
import static com.ahujaBros.utils.Constants.KEY_SUBITEMS;
import static com.ahujaBros.utils.Constants.KEY_TITLE;
import static com.ahujaBros.utils.Constants.UNSORTED;

/**
 * Created by insonix on 25/5/17.
 */

public class CategoryDetailFragment extends BaseFragment implements CategoryView, View.OnClickListener, SortItemClickListener, AddtoCartItemGenricClickListener {


    private static final String TAG = "test";
    @BindView(R.id.category_imgs)
    RecyclerView categoryImgs;
    @BindView(R.id.filter_fab)
    FloatingActionButton filterFab;
    @BindView(R.id.relative_data)
    RelativeLayout relativeData;
    @BindView(R.id.select_tabs)
    TabLayout selectTabs;
    Unbinder unbinder;
    @BindView(R.id.no_data)
    TextView noData;
    @BindView(R.id.filter_sort)
    FloatingActionButton filterSort;
    @BindView(R.id.filter)
    FloatingActionButton filter;
    private CategoryAdapter adapter;
    private LayoutAnimationController controller;
    CategoryDetailPresenter mPresenter;
    ArrayList<ProductsItem> mProducts;
    private ArrayList<SubCatItem> mTitles;
    private boolean isFabOpen = false;
    Dialog dialog;
    String[] sortItems = {"Popularity", "Price-Low to High", "Price-High to Low", "SetPrice-Low to High", "SetPrice-High to Low", "Trending"};
    private String maxValues, minValues;
    private List<String> supplierSpinnerList = new ArrayList<>();
    private Events.CategoriesSearchedEvent searchItem;
    private Events.CategoryEvent categoryItem;
    private ArrayList<SubGroupItem> mSubGroupTitles;
    private AddToolbarTitle mTitleListener;
    private String tabTitle="Ahuja Bros";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener = ((MainActivity) getActivity());
    }

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        ((MainActivity)getActivity()).showUpButton();
        initRecyclerView();
        getCategoryDetails();
        initRecyclerView();             //To refresh tab titles
        filterSort.setOnClickListener(this);
        return view;
    }

    private void getCategoryDetails() {
        if (getArguments() != null) {
            mTitleListener.changeToolbarTitle(getArguments().getString(KEY_TITLE));
            mPresenter = new CategoryDetailPresenterImpl(getActivity(), this);
            mPresenter.getCategoryDetails(getArguments().getString(KEY_CATID), getArguments().getString(KEY_SUBCATID), getArguments().getString(KEY_DEPTTID), String.valueOf(UNSORTED));
            mTitles = getArguments().getParcelableArrayList(KEY_SUBITEMS);
            setTabs(mTitles, null);
        } else if (GlobalBus.getBus().getStickyEvent(Events.CategoriesSearchedEvent.class) != null && MainActivity.searchDone==true) {
            MainActivity.searchDone=false;
            mTitleListener.changeToolbarTitle(getResources().getString(R.string.app_name));
            filterFab.setVisibility(View.GONE);
            searchItem = GlobalBus.getBus().getStickyEvent(Events.CategoriesSearchedEvent.class);
            if (searchItem.getListFound().size() > 0) {
                refreshSearchData(searchItem.getListFound());
            } else {
                categoryImgs.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                filterFab.setVisibility(View.GONE);
            }
        } else {
            categoryItem = GlobalBus.getBus().getStickyEvent(Events.CategoryEvent.class);
            mTitleListener.changeToolbarTitle(categoryItem.getmGroupName());
            mPresenter = new CategoryDetailPresenterImpl(getActivity(), this);
            mPresenter.getCategoryDetails(categoryItem.getmGroupId(), categoryItem.getmSubGroupId(), categoryItem.getmCategoryId(), String.valueOf(UNSORTED));
            mSubGroupTitles = categoryItem.getTitles();
            setTabs(null, mSubGroupTitles);
        }
    }

    private void refreshSearchData(List<ProductsItem> listFound) {
        adapter = new CategoryAdapter((MainActivity) getActivity(), listFound, this, this, getResources().getString(R.string.app_name));
        categoryImgs.setAdapter(adapter);
//        GlobalBus.getBus().removeStickyEvent(Events.CategoriesSearchedEvent.class);
    }

    private void setTabs(final ArrayList<SubCatItem> titles, final ArrayList<SubGroupItem> mSubTitles) {
        selectTabs.removeAllTabs();
        //Adding the tabs using addTab() method
        if (titles != null) {
            for (int i = 0; i < titles.size(); i++) {
                selectTabs.addTab(selectTabs.newTab().setText(titles.get(i).getName()));
                selectTabs.setTabGravity(TabLayout.GRAVITY_FILL);
            }
            tabTitle = titles.get(0).getName();
            selectTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    tabTitle = titles.get(tab.getPosition()).getName();
                    mPresenter.getCategoryDetails(getArguments().getString(KEY_CATID), titles.get(tab.getPosition()).getSubGroupId(), getArguments().getString(KEY_DEPTTID), String.valueOf(UNSORTED));
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    tabTitle = titles.get(tab.getPosition()).getName();
                    mPresenter.getCategoryDetails(getArguments().getString(KEY_CATID), titles.get(tab.getPosition()).getSubGroupId(), getArguments().getString(KEY_DEPTTID), String.valueOf(UNSORTED));
                }
            });

        } else {
            for (int i = 0; i < mSubTitles.size(); i++) {
                Log.i(TAG, "setTabs: " + mSubTitles.size());
                selectTabs.addTab(selectTabs.newTab().setText(mSubTitles.get(i).getSubGroupName()));
                selectTabs.setTabGravity(TabLayout.GRAVITY_FILL);
            }
            tabTitle = mSubTitles.get(0).getSubGroupName();
            selectTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    tabTitle = mSubTitles.get(tab.getPosition()).getSubGroupName();
                    mPresenter.getCategoryDetails(categoryItem.getmGroupId(), mSubTitles.get(tab.getPosition()).getSubGroupId(), categoryItem.getmCategoryId(), String.valueOf(UNSORTED));
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    tabTitle = mSubTitles.get(tab.getPosition()).getSubGroupName();
                    mPresenter.getCategoryDetails(categoryItem.getmGroupId(), mSubTitles.get(tab.getPosition()).getSubGroupId(), categoryItem.getmCategoryId(), String.valueOf(UNSORTED));
                }
            });


        }
        filterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
    }

    private void initRecyclerView() {
        Log.i(TAG, "initRecyclerView: " + tabTitle);
        mProducts = new ArrayList<>();
        adapter = new CategoryAdapter((MainActivity) getActivity(), mProducts, this, this, tabTitle);
        categoryImgs.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        categoryImgs.setLayoutAnimation(initAnim());
        categoryImgs.setAdapter(adapter);
    }

    private LayoutAnimationController initAnim() {
        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        controller = new LayoutAnimationController(set, 0.5f);
        return controller;
    }

    @Override
    public void onResume() {
        super.onResume();
//        getCategoryDetails();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void refreshData(CategoryDetailResponse categoryDetailResponse, List sampleList) {
        mProducts.clear();
        mProducts.addAll(categoryDetailResponse.getData().getProducts());
        adapter.notifyDataSetChanged();
        categoryImgs.setVisibility(View.VISIBLE);
        noData.setVisibility(View.GONE);
        filterFab.setVisibility(View.VISIBLE);
        Log.i(TAG, "refreshData: " + sampleList.size());
        supplierSpinnerList = new ArrayList<>(new LinkedHashSet<>(sampleList));
    }

    @Override
    public void onNoData(String s) {
        categoryImgs.setVisibility(View.GONE);
        noData.setVisibility(View.VISIBLE);
        filterFab.setVisibility(View.GONE);
    }

    public static CategoryDetailFragment newInstance(ArrayList<SubCatItem> list, String title, String selectedGroupID, String selectedChildId, String depttID) {
        CategoryDetailFragment fragmentCategory = new CategoryDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_FRAGTITLE, title);
        args.putString(KEY_CATID, selectedGroupID);
        args.putString(KEY_SUBCATID, selectedChildId);
        args.putString(KEY_DEPTTID, depttID);
        args.putParcelableArrayList(KEY_SUBITEMS, list);
        fragmentCategory.setArguments(args);
        return fragmentCategory;
    }

    public void animateFAB() {
        if (isFabOpen) {

            filterFab.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_backward));
            filter.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close));
            filterSort.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close));
            filter.setClickable(false);
            filterSort.setClickable(false);
            isFabOpen = false;
            Log.d("Fab", "close");

        } else {

            filterFab.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forward));
            filter.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open));
            filterSort.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open));
            filter.setClickable(true);
            filterSort.setClickable(true);
            isFabOpen = true;
            Log.d("Fab", "open");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_sort:
                dialog = new Dialog(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                dialog.setContentView(R.layout.sort_list);
                ListView sortList = (ListView) dialog.findViewById(R.id.sort_options_list);
                sortList.setAdapter(new SortListAdapter(getActivity(), sortItems, this));
                dialog.show();
                break;
        }
    }

    @Override
    public void onListItemClick(int pos) {
        Log.i(TAG, "onHomeListItemClick: ");
        if (dialog.isShowing())
            dialog.dismiss();
        if (getArguments() != null) {
            mPresenter.getCategoryDetails(getArguments().getString(KEY_CATID), getArguments().getString(KEY_SUBCATID), getArguments().getString(KEY_DEPTTID), String.valueOf(pos));
        } else {
            mPresenter.getCategoryDetails(categoryItem.getmGroupId(), categoryItem.getmSubGroupId(), categoryItem.getmCategoryId(), String.valueOf(UNSORTED));
        }
    }

    @OnClick(R.id.filter)
    public void onViewClicked() {
        Log.i(TAG, "onClick: sssfilter");
        final Dialog dialog = new Dialog(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
        dialog.setContentView(R.layout.filter_dialog);
        dialog.setTitle("Filter Products");
        final RangeSeekBar rangeSeekbar = (RangeSeekBar) dialog.findViewById(R.id.valuerangeSeekbar);
        final Spinner supplierSpinner = (Spinner) dialog.findViewById(R.id.spinner_supplier);
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(MyApplication.getInstance(), R.layout.simple_spinner_item, supplierSpinnerList);
        supplierSpinner.setAdapter(spinnerAdapter);
        final EditText tvMin = (EditText) dialog.findViewById(R.id.min_price);
        final EditText tvMax = (EditText) dialog.findViewById(R.id.max_price);
        Button submit = (Button) dialog.findViewById(R.id.filter_bttn);
        final String minVal = tvMin.getText().toString().trim().equalsIgnoreCase("") ? "0" : tvMin.getText().toString().trim();
        final String maxVal = tvMax.getText().toString().trim().equalsIgnoreCase("") ? "20000" : tvMax.getText().toString().trim();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (getArguments() != null) {
                    mPresenter.filterItems(getArguments().getString(KEY_CATID), getArguments().getString(KEY_DEPTTID), getArguments().getString(KEY_SUBCATID), supplierSpinner.getSelectedItem().toString(), minVal, maxVal, UNSORTED);
                } else {
                    mPresenter.filterItems(categoryItem.getmGroupId(), categoryItem.getmCategoryId(), categoryItem.getmSubGroupId(), supplierSpinner.getSelectedItem().toString(), minVal, maxVal, UNSORTED);
                }
            }
        });


        rangeSeekbar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                maxValues = maxValue.toString();
                minValues = minValue.toString();
                tvMax.setText(maxValues);
                tvMin.setText(minValues);
            }
        });


        tvMin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                text_maxPrice.setText(maxValues.toString());
//                text_minPrice.setText(minValues.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    minValues = s.toString().trim();
                    try {
                        int aa = Integer.parseInt(minValues);
                        rangeSeekbar.setSelectedMinValue(aa);
                    } catch (Exception e) {

                    }
                }
            }
        });
        tvMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    maxValues = s.toString().trim();
                    try {
                        int aa = Integer.parseInt(maxValues);
                        rangeSeekbar.setSelectedMaxValue(aa);
                    } catch (Exception e) {

                    }

                }
            }
        });

        dialog.show();
    }


    @Override
    public void onItemClick(ProductsItem pos) {
        mPresenter.addToCart(pos.getId(), pos.getQty());
    }

    @Override
    public void showError(String str) {
        super.showError(str);
        int i = 0;
        if (GlobalBus.getBus().getStickyEvent(Events.CartCountEvent.class) != null) {
            i = GlobalBus.getBus().getStickyEvent(Events.CartCountEvent.class).getCount();
            GlobalBus.getBus().postSticky(new Events.CartCountEvent(i + 1));
        } else {
            i = MyPreferences.getInstance(getActivity()).getInt(MyPreferences.Key.CART_COUNT);
            i++;        //increasing value of i
            GlobalBus.getBus().postSticky(new Events.CartCountEvent(i));
        }
        //Adding 1 to cart count everytime add to cart is hit.
        ((MainActivity) getActivity()).updateCartCount();
    }

}
