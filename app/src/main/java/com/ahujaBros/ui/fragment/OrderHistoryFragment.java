package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.OrderHistoryAdapter;
import com.ahujaBros.model.order.OrderHistoryHeader;
import com.ahujaBros.model.order.ProductInfo;
import com.ahujaBros.presenter.OrderHistoryPresenter;
import com.ahujaBros.presenter.OrderHistoryPresenterImpl;
import com.ahujaBros.ui.OrderHistoryView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.src.ahujaBros.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by insonix on 12/6/17.
 */

public class OrderHistoryFragment extends BaseFragment implements OrderHistoryView,RecyclerItemGenricClickListener {


    @BindView(R.id.order_history_list)
    RecyclerView orderHistoryList;
    Unbinder unbinder;
    private OrderHistoryAdapter adapter;
    OrderHistoryPresenter mPresenter;
    private HashMap<String, List<ProductInfo>> mListDataChild;
    private ArrayList<OrderHistoryHeader> mListDataHeader;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener= ((MainActivity)getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        mTitleListener.changeToolbarTitle(getResources().getString(R.string.ordr_history));
        setHasOptionsMenu(true);
        mPresenter=new OrderHistoryPresenterImpl(this);
        return view;

    }

    private void initRecyclerView() {
        adapter = new OrderHistoryAdapter(new ArrayList<OrderHistoryHeader>(), this);
        orderHistoryList.setLayoutManager(new LinearLayoutManager(getActivity()));
        orderHistoryList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getOrders();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.search).setVisible(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLoadingOrders(ArrayList<OrderHistoryHeader> listDataHeader, HashMap<String, List<ProductInfo>> listDataChild) {
        mListDataChild=new HashMap<>();
        mListDataHeader=new ArrayList<>();
        mListDataChild.putAll(listDataChild);
        mListDataHeader.addAll(listDataHeader);
        adapter.refresh(mListDataHeader);
    }

    @Override
    public void onItemClick(int pos) {
        GlobalBus.getBus().postSticky(new Events.OrderDetailEvent(mListDataChild.get(mListDataHeader.get(pos).getOrderId())));
        CommonUtils.addFragment(getActivity(),new OrderDetailFragment(),false,"Order_Detail",null);
    }
}
