package com.ahujaBros.ui;

import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.cart.OrderResponse;
import com.ahujaBros.ui.base.BaseView;

/**
 * Created by insonix on 5/6/17.
 */

public interface CartView extends BaseView{


    void onFetchCart(MyCart cart);

    void onSuccessRemove(MyCart cart);

    void noDataAvailable();

    void onOrderSuccess(OrderResponse response);
}
