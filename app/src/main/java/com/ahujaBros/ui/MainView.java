package com.ahujaBros.ui;

import com.ahujaBros.model.Search.ProductFoundResponse;
import com.ahujaBros.model.generic.BadgeResponse;
import com.ahujaBros.model.menu.MenuResponse;
import com.ahujaBros.ui.base.BaseView;

import java.util.List;

/**
 * Created by insonix on 19/5/17.
 */

public interface MainView extends BaseView{

    void getMenuCategories(MenuResponse categoryResponse);

    void updateBadgeCount(BadgeResponse response);

    void searchedData(ProductFoundResponse response);

    void searchKeywordsList(List<String> data);
}
