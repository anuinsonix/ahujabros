package com.ahujaBros.ui;

import com.ahujaBros.model.CategoryDetail.CategoryDetailResponse;
import com.ahujaBros.ui.base.BaseView;

import java.util.List;

/**
 * Created by insonix on 25/5/17.
 */

public interface CategoryView extends BaseView {

    void refreshData(CategoryDetailResponse categoryDetailResponse, List sampleList);

    void onNoData(String s);
}
