package com.ahujaBros.ui;

import com.ahujaBros.model.home.HomeResponse;

/**
 * Created by insonix on 23/5/17.
 */

public interface HomeView {


    void refreshData(HomeResponse response);

}
