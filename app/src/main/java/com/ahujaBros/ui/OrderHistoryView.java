package com.ahujaBros.ui;

import com.ahujaBros.model.order.OrderHistoryHeader;
import com.ahujaBros.model.order.ProductInfo;
import com.ahujaBros.ui.base.BaseView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by insonix on 13/6/17.
 */

public interface OrderHistoryView extends BaseView {


    void onLoadingOrders(ArrayList<OrderHistoryHeader> listDataHeader, HashMap<String, List<ProductInfo>> listDataChild);

}
